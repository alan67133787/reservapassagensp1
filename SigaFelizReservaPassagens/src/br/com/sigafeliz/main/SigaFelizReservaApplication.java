package br.com.sigafeliz.main;

import br.com.sigafeliz.exceptions.AssentoInformadoIndisponivelException;
import br.com.sigafeliz.exceptions.NaoHaAssentosDisponiveisException;
import br.com.sigafeliz.exceptions.NumeroPoltronaInvalidoException;
import br.com.sigafeliz.exceptions.TipoAssentoInvalidoException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Programa principal do software para comercialização de venda de passagens
 *  
 * @author Alan Alves da Silva -  RA 20486574
 * @author Cesar Francisco Ferreira -  RA 20804959
 * @author Camila de Souza Ramos - RA 20755079
 * @author Beatriz Ogura Hanashiro - RA 1125856
 * @author Filipe Costa - RA 20694230
 * @author Edvan Jenorimo - RA 20795900
 */
public class SigaFelizReservaApplication {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        
        int opcaoMenu = 0;
        
        int opcaoMenuTemp = 0;
        
        int poltronaDesejada = 0;
        
        String tipoAssento = null;
        
        Scanner in = null;
        
        StringBuilder stringBuilder = null;
        
        Onibus onibus = new Onibus();
        onibus.setPlacaOnibus("ZTO-0055");
        onibus.setOrigem("São Paulo / SP");
        onibus.setDestino("Manaus / AM");
        onibus.setPlataformaEmbarque("AAA - 12");
        try{
            onibus.setDataHoraViagem(new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse("01/12/2015 15:00:00"));
        }catch(ParseException pex){}
        
        /*
            Cria a relação de assentos para JANELA
        */
        onibus.setAssentosJanela(new Assento[24]);
        for(int i=0; i<onibus.getAssentosJanela().length; i++){
            onibus.getAssentosJanela()[i] = new Assento(false, i+1,'J'); //Pares são JANELAS e Impares Corredor
        }
        
        /*
        Cria a relação de assentos para CORREDOR
        */
        onibus.setAssentosCorredor(new Assento[24]);
        for(int i=0; i<onibus.getAssentosCorredor().length; i++){
            onibus.getAssentosCorredor()[i] = new Assento(false, i+1,'C'); //Pares são JANELAS e Impares Corredor
        }
           
        while(true){
            switch(opcaoMenu){
                case 0:
                    /*
                        Menu principal da aplicação
                    */
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("\n\n\n\n\n\n\n\n\n\n\n");
                    stringBuilder.append("Siga Feliz Tranportes e Turismo - SEJA BEM VINDO!!! \n\n");
                    stringBuilder.append("MENU INICIAL ­ SVPA 1.0 SISTEMA DE VENDA DE PASSAGENS\n\n");
                    stringBuilder.append("Opções disponíveis no menu: \n\n");
                    stringBuilder.append("[1] - Vender Passagem\n");
                    stringBuilder.append("[2] - Mostrar mapa de ocupação\n");
                    stringBuilder.append("[3] - Encerrar\n\n");
                    stringBuilder.append("Data\\Hora: "+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())+"\n\n\n ");

                    System.out.println(stringBuilder.toString());
                    System.out.print("Digite a opção[ENTER]: ");
                    in = new Scanner(System.in);
                    opcaoMenu = in.nextInt();
                    break;
                case 1:
                    /*
                        Modulo responsavel pela reserva de passagens
                    */
                    
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("\n\n\n\n\n\n\n\n\n\n\n");
                    stringBuilder.append("Siga Feliz Tranportes e Turismo - SEJA BEM VINDO!!! \n\n");
                    stringBuilder.append("VENDAS DE PASSAGENS ­ SVPA 1.0 SISTEMA DE VENDA DE PASSAGENS - Digite [0] para voltar ao menu.\n\n");
                    stringBuilder.append("\t Digite a Poltrona Desejada[ENTER]: ");
                    
                    System.out.print(stringBuilder.toString());
                    
                    in = new Scanner(System.in);
                    poltronaDesejada = in.nextInt();
                    
                    System.out.print("\t Digite a posição do assento (J ­ Janela ou C ­ Corredor)[ENTER]: ");
                    in = new Scanner(System.in);
                    tipoAssento = in.next();
                    
                    if(poltronaDesejada != 0){
                        try{
                            Assento assentoReservado = SigaFelizReservaMenuUtils.venderPassagens(onibus, tipoAssento != null ? tipoAssento.toUpperCase().toCharArray()[0] : 'N', poltronaDesejada);
                            System.out.println("\t Reserva efetuada com sucesso!!!");

                            System.out.print("\t Digite [4] Exibir Bilhete ou [0] Menu Inicial[ENTER]: ");
                            in = new Scanner(System.in);
                            opcaoMenu = in.nextInt();
                            
                            if(opcaoMenu == 4){
                                /*
                                    Modulo adicional da venda de passagens, realiza a impressão do comprovante
                                    de reserva
                                */
                                
                                stringBuilder = new StringBuilder();
                                stringBuilder.append("\n\n\n\n\n\n\n\n\n\n\n");
                                stringBuilder.append("Siga Feliz Tranportes e Turismo - Data\\Hora: "+ new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(assentoReservado.getDataHoraEmissao())+" \n\n");
                                stringBuilder.append("*************** COMPROVANTE DE RESERVA *********************\n\n\n");
                                stringBuilder.append("Nome do Cliente: " + (assentoReservado != null ? assentoReservado.getNomePessoa() : "NAO INFORMADO") + "\n");
                                stringBuilder.append("Poltrona: " + assentoReservado.getNumeroPoltrona()+ " - " + (assentoReservado.getTipoAcento() == 'J' ? "JANELA":"CORREDOR") + "\n\n");
                                stringBuilder.append("Data\\Hora Viagem: " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(onibus.getDataHoraViagem())+ "\n");
                                stringBuilder.append("Plataforma de embarque: " + onibus.getPlataformaEmbarque()+ "\n");
                                stringBuilder.append("OBS: Compareça na plataforma com 30 minutos de antecedencia\n");
                                stringBuilder.append("*************** NÃO VALE COMO COMPROVANTE FISCAL *********************\n\n");
                                
                                System.out.println(stringBuilder.toString());
                                System.out.print("\t Digite [0] Menu Inicial: ");
                                in = new Scanner(System.in);
                                opcaoMenu = in.nextInt();
                            }
                        }catch(NaoHaAssentosDisponiveisException ndiex){
                            System.out.println("\t\t *** " + ndiex.getMessage() + " ***");
                            System.out.print("Para voltar ao menu inicial digite [0] ou digite [1] para continuar[ENTER]: ");
                            
                            in = new Scanner(System.in);
                            opcaoMenuTemp = in.nextInt();
                        }catch(NumeroPoltronaInvalidoException nex){
                            System.out.println("\t\t *** " + nex.getMessage() + " ***");
                            System.out.print("Para voltar ao menu inicial digite [0] ou digite [1] para continuar[ENTER]: ");
                            
                            in = new Scanner(System.in);
                            opcaoMenuTemp = in.nextInt();
                        }catch(AssentoInformadoIndisponivelException asex){ 
                            System.out.println("\t\t *** " + asex.getMessage() + " ***");
                            System.out.print("Para voltar ao menu inicial digite [0] ou digite [1] para continuar[ENTER]: ");
                            
                            in = new Scanner(System.in);
                            opcaoMenuTemp = in.nextInt();
                        }catch(TipoAssentoInvalidoException tpex){
                            System.out.println("\t\t *** " + tpex.getMessage() + " ***");
                            System.out.print("Para voltar ao menu inicial digite [0] ou digite [1] para continuar[ENTER]: ");
                            
                            in = new Scanner(System.in);
                            opcaoMenuTemp = in.nextInt();
                        }
                        
                        if(opcaoMenuTemp == 0){
                            opcaoMenu = opcaoMenuTemp;
                        }
                    } else {
                        opcaoMenu = poltronaDesejada;
                    }
                    break;
                case 2:
                    /*
                        Modulo responsavel por exibir o mapa de ocupação do onibus
                    */
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("\n\n\n\n\n\n\n\n\n\n\n");
                    stringBuilder.append("Siga Feliz Tranportes e Turismo - SEJA BEM VINDO!!! \n\n");
                    stringBuilder.append("MAPA DE OCUPAÇÃO ­ SVPA 1.0 SISTEMA DE VENDA DE PASSAGENS - Digite [0] para voltar ao menu.\n\n");                   
                    
                    for(int i=0; i<24; i++){
                        
                        Assento assentosJanelas[] = onibus.getAssentosJanela();
                        
                        Assento assentosCorredor[] = onibus.getAssentosCorredor();
                        
                        stringBuilder.append(assentosJanelas[i].getTipoAcento() + "-" + (assentosJanelas[i].getNumeroPoltrona() <= 9 ? "0" + assentosJanelas[i].getNumeroPoltrona() : assentosJanelas[i].getNumeroPoltrona() ) +(assentosJanelas[i].isOcupado() ? "-OCUPADO" : "-LIVRE") + " \t");
                        
                        stringBuilder.append(assentosCorredor[i].getTipoAcento() + "-" + (assentosCorredor[i].getNumeroPoltrona() <= 9 ? "0" + assentosCorredor[i].getNumeroPoltrona() : assentosCorredor[i].getNumeroPoltrona() ) +(assentosCorredor[i].isOcupado() ? "-OCUPADO" : "-LIVRE") + " \t");

                        stringBuilder.append("\n");
                    }
                 
                    stringBuilder.append("\n\n J-JANELA ou C-CORREDOR");
                    
                    System.out.println(stringBuilder.toString());

                    System.out.print("\t Digite [0] Menu Inicial: ");
                    in = new Scanner(System.in);
                    opcaoMenu = in.nextInt();
                    break;
                    
                case 3:
                    /*
                        Finaliza a aplicação
                    */
                    System.out.println("OBRIGADO POR UTILIZAR O SVPA 1.0");
                    System.exit(0);
                default:
                    opcaoMenu = 1;
            }
        }
    } 
}
