package br.com.sigafeliz.main;

import java.util.Date;

/**
 * Classe responsável por representar Onibus que será utilizado para iteração na classe
 * principal SigaFelizReservaApplication
 * 
 * @author Alan Alves da Silva -  RA 20486574
 * @author Cesar Francisco Ferreira -  RA 20804959
 * @author Camila de Souza Ramos - RA 20755079
 * @author Beatriz Ogura Hanashiro - RA 1125856
 * @author Filipe Costa - RA 20694230
 * @author Edvan Jenorimo - RA 20795900
 */
public class Onibus {
    
    private String placaOnibus;
    
    private String origem;
    
    private String destino;
    
    private Assento assentosJanela[];
    
    private Assento assentosCorredor[];
    
    private Date dataHoraViagem;
    
    private String plataformaEmbarque;

    public String getPlacaOnibus() {
        return placaOnibus;
    }

    public void setPlacaOnibus(String placaOnibus) {
        this.placaOnibus = placaOnibus;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Assento[] getAssentosJanela() {
        return assentosJanela;
    }

    public void setAssentosJanela(Assento[] assentosJanela) {
        this.assentosJanela = assentosJanela;
    }

    public Assento[] getAssentosCorredor() {
        return assentosCorredor;
    }

    public void setAssentosCorredor(Assento[] assentosCorredor) {
        this.assentosCorredor = assentosCorredor;
    }
    
    public Date getDataHoraViagem() {
        return dataHoraViagem;
    }

    public void setDataHoraViagem(Date dataHoraViagem) {
        this.dataHoraViagem = dataHoraViagem;
    }

    public String getPlataformaEmbarque() {
        return plataformaEmbarque;
    }

    public void setPlataformaEmbarque(String plataformaEmbarque) {
        this.plataformaEmbarque = plataformaEmbarque;
    }
}
