package br.com.sigafeliz.main;

import java.util.Date;

/**
 * Possui informações referente a localização do assento inclusive
 * dados de cliente em caso de poltrona reservada
 * 
 * @author Alan Alves da Silva -  RA 20486574
 * @author Cesar Francisco Ferreira -  RA 20804959
 * @author Camila de Souza Ramos - RA 20755079
 * @author Beatriz Ogura Hanashiro - RA 1125856
 * @author Filipe Costa - RA 20694230
 * @author Edvan Jenorimo - RA 20795900
 */
public class Assento {
    
    private String nomePessoa;
    
    private Date dataHoraEmissao;
    
    private boolean ocupado;
    
    private int numeroPoltrona;
    
    /*
      J - Janela
      C - Corredor
    */
    private char tipoAcento;

    public Assento(boolean ocupado, int numeroPoltrona, char tipoAcento) {
        this.ocupado = ocupado;
        this.numeroPoltrona = numeroPoltrona;
        this.tipoAcento = tipoAcento;
    }

    public boolean isOcupado() {
        return ocupado;
    }

    public void setOcupado(boolean ocupado) {
        this.ocupado = ocupado;
    }

    public int getNumeroPoltrona() {
        return numeroPoltrona;
    }

    public void setNumeroPoltrona(int numeroPoltrona) {
        this.numeroPoltrona = numeroPoltrona;
    }

    public char getTipoAcento() {
        return tipoAcento;
    }

    public void setTipoAcento(char tipoAcento) {
        this.tipoAcento = tipoAcento;
    }

    public String getNomePessoa() {
        return nomePessoa;
    }

    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    }

    public Date getDataHoraEmissao() {
        return dataHoraEmissao;
    }

    public void setDataHoraEmissao(Date dataHoraEmissao) {
        this.dataHoraEmissao = dataHoraEmissao;
    }

}
