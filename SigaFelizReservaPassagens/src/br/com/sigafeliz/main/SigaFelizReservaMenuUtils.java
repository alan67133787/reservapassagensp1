package br.com.sigafeliz.main;

import br.com.sigafeliz.exceptions.AssentoInformadoIndisponivelException;
import br.com.sigafeliz.exceptions.NaoHaAssentosDisponiveisException;
import br.com.sigafeliz.exceptions.NumeroPoltronaInvalidoException;
import br.com.sigafeliz.exceptions.TipoAssentoInvalidoException;
import java.util.Date;
import java.util.Scanner;

/**
 * Classe utilitaria responsavel para validar a regra de aceitação
 * na reserva do assento.
 * 
 * 
 * @author Alan Alves da Silva -  RA 20486574
 * @author Cesar Francisco Ferreira -  RA 20804959
 * @author Camila de Souza Ramos - RA 20755079
 * @author Beatriz Ogura Hanashiro - RA 1125856
 * @author Filipe Costa - RA 20694230
 * @author Edvan Jenorimo - RA 20795900
 */
public class SigaFelizReservaMenuUtils {
        
    public static Assento venderPassagens(Onibus onibus, char tipoAssento, int poltronaDesejada) throws NaoHaAssentosDisponiveisException, NumeroPoltronaInvalidoException, AssentoInformadoIndisponivelException, TipoAssentoInvalidoException{
        
        if(poltronaDesejada < 1 || poltronaDesejada > 24){
            throw new NumeroPoltronaInvalidoException();
        } 
        
        if(tipoAssento != 'J' && tipoAssento != 'C'){
            throw new TipoAssentoInvalidoException();
        }
        
        Assento assentosArray[] = null;
        
        if(tipoAssento == 'J'){
            assentosArray = onibus.getAssentosJanela();
        } else {
            assentosArray = onibus.getAssentosCorredor();
        }

        for(Assento assentos : assentosArray){
            
            if(assentos.getNumeroPoltrona() == poltronaDesejada && assentos.isOcupado()){
                throw new AssentoInformadoIndisponivelException();
            }
            
            if(assentos.getNumeroPoltrona() == poltronaDesejada && !assentos.isOcupado()){
                assentos.setOcupado(true);
                
                System.out.print("\t Digite o nome do cliente[ENTER]: ");
                Scanner in = new Scanner(System.in);
                
                String nome = in.nextLine();
                assentos.setNomePessoa(nome);
                assentos.setDataHoraEmissao(new Date());
                return assentos;
            }
        }
        
        return null;
    }
}
