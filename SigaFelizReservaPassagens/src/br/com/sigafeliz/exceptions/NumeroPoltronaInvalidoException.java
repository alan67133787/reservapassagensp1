package br.com.sigafeliz.exceptions;

/**
 * @author Alan Alves da Silva -  RA 20486574
 * @author Cesar Francisco Ferreira -  RA 20804959
 * @author Camila de Souza Ramos - RA 20755079
 * @author Beatriz Ogura Hanashiro - RA 1125856
 * @author Filipe Costa - RA 20694230
 * @author Edvan Jenorimo - RA 20795900
 */
public class NumeroPoltronaInvalidoException extends Exception{
    public NumeroPoltronaInvalidoException(){
        super("Número de poltrona inválido, escolha um número entre 1 e 24");
    }
}
