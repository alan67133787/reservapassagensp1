/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sigafeliz.exceptions;

/**
 *
 * @author Alan Alves da Silva -  RA 20486574
 * @author Cesar Francisco Ferreira -  RA 20804959
 * @author Camila de Souza Ramos - RA 20755079
 * @author Beatriz Ogura Hanashiro - RA 1125856
 * @author Filipe Costa - RA 20694230
 * @author Edvan Jenorimo - RA 20795900
 */
public class TipoAssentoInvalidoException extends Exception{
    
    public TipoAssentoInvalidoException(){
        super("\"Posição inválida, escolha J ­ Janela ou C ­ Corredor");
    }
    
}
